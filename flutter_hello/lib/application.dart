import 'package:flutter/material.dart';
import 'package:flutter_hello/page/onboarding_page.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: '/',
      routes: {
        '/onboarding': (context) => OnboardingPage(),
      },
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: OnboardingPage(),
    );
  }
}
