class User {
  late int id;
  late String name;
  late String avatar;
  late int age;
  late String address;

  User({
    required this.id,
    required this.name,
    required this.avatar,
    required this.age,
    required this.address,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    avatar = json['avatar'];
    age = json['age'];
    address = json['address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['avatar'] = this.avatar;
    data['age'] = this.age;
    data['address'] = this.address;
    return data;
  }
}
