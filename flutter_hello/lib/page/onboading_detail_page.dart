import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_hello/model/user.dart';

class OnboardingDetailPage extends StatelessWidget {
  final User user;

  OnboardingDetailPage({required this.user});
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final itemSize = size.width * 2 / 5.0;
    final fontSize = 16.0;
    return Scaffold(
      appBar: AppBar(
        title: Text(user.name),
      ),
      body: Container(
        alignment: Alignment.topLeft,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Card(
              elevation: 0.5,
              child: Container(
                padding: EdgeInsets.all(8),
                width: itemSize,
                height: itemSize,
                child: Hero(
                  tag: 'Hero-${user.id}',
                  child: FittedBox(
                    alignment: Alignment.center,
                    child: Image.asset(user.avatar),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    user.name,
                    style: TextStyle(color: Colors.purple),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text('${user.age}'),
                  SizedBox(
                    height: 8,
                  ),
                  Text('${user.address}'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
