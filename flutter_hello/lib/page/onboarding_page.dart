import 'package:flutter/material.dart';
import 'package:flutter_hello/page/onboading_detail_page.dart';
import 'package:flutter_hello/page/onboarding_page_store.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class OnboardingPage extends StatelessWidget {
  final OnboardingPageStore store = OnboardingPageStore();

  _fetch() {
    store.fetch();
  }

  @override
  Widget build(BuildContext context) {
    _fetch();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text('Flutter Module'),
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.grid_3x3_outlined)),
          IconButton(
            onPressed: () => _fetch(),
            icon: Icon(Icons.refresh_outlined),
          )
        ],
      ),
      body: Observer(builder: (_) {
        final users = store.users;

        final state = store.state;
        if (state != StoreState.Finish) {
          return Center(
            child: CircularProgressIndicator(
              color: Colors.purple,
            ),
          );
        }
        return GridView.builder(
          padding: EdgeInsets.all(8),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 8,
            crossAxisSpacing: 8,
            childAspectRatio: 1,
          ),
          itemBuilder: (context, index) {
            final user = users[index];
            return InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return OnboardingDetailPage(user: user);
                  },
                ));
              },
              child: Card(
                elevation: 2,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Hero(
                    tag: 'Hero-${user.id}',
                    child: FittedBox(
                      alignment: Alignment.center,
                      child: Image.asset(user.avatar),
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
            );
          },
          itemCount: users.length,
        );
      }),
    );
  }
}
