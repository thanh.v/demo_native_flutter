import 'dart:convert';

import 'package:flutter_hello/model/user.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter/services.dart' show rootBundle;
part 'onboarding_page_store.g.dart';

class OnboardingPageStore = _OnboardingPageStoreBase with _$OnboardingPageStore;

enum StoreState { Init, Loading, Finish }

abstract class _OnboardingPageStoreBase with Store {
  @observable
  StoreState state = StoreState.Init;

  @observable
  List<User> users = [];

  @action
  Future fetch() async {
    if (state == StoreState.Loading) {
      return;
    }

    state = StoreState.Loading;

    await Future.delayed(Duration(seconds: 2));
    String data = await rootBundle.loadString('assets/dssv.json');

    final json = jsonDecode(data);

    users = (json as List).map((e) => User.fromJson(e)).toList();

    state = StoreState.Finish;
  }
}
