//
//  ViewController.swift
//  ios_hello_world
//
//  Created by Thanh Vu on 01/10/2021.
//

import UIKit
import Flutter

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Native iOS";
        // Do any additional setup after loading the view.
    }

    @IBAction @objc func presentHelloFlutter(_ sender: Any) {
        let flutterEngine = (UIApplication.shared.delegate as! AppDelegate).flutterEngine
        
        flutterEngine.run(withEntrypoint: nil, initialRoute: "/onboarding")
        
        let flutterViewController =
            FlutterViewController(engine: flutterEngine, nibName: nil, bundle: nil)
        flutterViewController.modalPresentationStyle = .overFullScreen
//        flutterViewController.navigationController?.modalPresentationStyle = UIModalPresentationStyle.currentContext
        
        present(flutterViewController, animated: true, completion: nil)
        
//        self.navigationController?.pushViewController(flutterViewController, animated: true)
    }
    
}

